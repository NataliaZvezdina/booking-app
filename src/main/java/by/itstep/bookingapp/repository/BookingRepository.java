package by.itstep.bookingapp.repository;

import by.itstep.bookingapp.entity.BookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<BookingEntity, Integer> {
}
