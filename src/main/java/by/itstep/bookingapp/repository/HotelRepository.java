package by.itstep.bookingapp.repository;

import by.itstep.bookingapp.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelRepository extends JpaRepository<HotelEntity, Integer> {
}
