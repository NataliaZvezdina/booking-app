package by.itstep.bookingapp.repository;

import by.itstep.bookingapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    UserEntity findByEmailIgnoreCase(String email);
}
