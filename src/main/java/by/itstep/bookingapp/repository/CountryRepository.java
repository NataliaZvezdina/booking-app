package by.itstep.bookingapp.repository;

import by.itstep.bookingapp.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<CountryEntity, Integer> {

    CountryEntity findByNameIgnoreCase(String name);
}
