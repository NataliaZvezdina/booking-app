package by.itstep.bookingapp.repository;

import by.itstep.bookingapp.entity.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {
}
