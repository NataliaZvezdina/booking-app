package by.itstep.bookingapp.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name = "rooms")
@Entity
public class RoomEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "type")
    private RoomType type;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private List<BookingEntity> bookings = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private HotelEntity hotel;
}
