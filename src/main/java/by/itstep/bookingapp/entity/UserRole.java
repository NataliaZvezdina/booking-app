package by.itstep.bookingapp.entity;

public enum UserRole {

    USER,
    ADMIN
}
