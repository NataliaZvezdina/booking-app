package by.itstep.bookingapp.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name = "hotels")
@Entity
public class HotelEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "stars")
    private Integer stars;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "price_per_night")
    private BigDecimal pricePerNight;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    private List<RoomEntity> rooms = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryEntity country;
}
