package by.itstep.bookingapp.entity;

public enum RoomType {

    SINGLE,
    DOUBLE,
    TRIPLE,
    QUADRUPLE
}
